﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SqlLite.First.Migrations
{
    public partial class Start : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Mail",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    From = table.Column<string>(type: "TEXT", nullable: true),
                    To = table.Column<string>(type: "TEXT", nullable: true),
                    Body = table.Column<string>(type: "TEXT", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mail", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Mail",
                columns: new[] { "Id", "Body", "CreatedOn", "From", "To" },
                values: new object[] { new Guid("ae5608c4-0812-4584-8382-d738542b05d0"), "Zdarova", new DateTime(2021, 3, 1, 22, 47, 19, 386, DateTimeKind.Local).AddTicks(8614), "Stepan", "Boris" });

            migrationBuilder.InsertData(
                table: "Mail",
                columns: new[] { "Id", "Body", "CreatedOn", "From", "To" },
                values: new object[] { new Guid("2e41d1af-5626-479d-8aa7-d94843621f0a"), "Privet", new DateTime(2021, 3, 1, 22, 47, 19, 387, DateTimeKind.Local).AddTicks(4062), "Samara", "Moscow" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Mail");
        }
    }
}
