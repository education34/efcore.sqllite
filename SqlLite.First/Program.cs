using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace SqlLite.First
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSystemd()
                .ConfigureServices((context, services) =>
                {
                    services.AddDbContext<DataStorage>(options =>
                    {
                        options.UseSqlite("Filename=My.db");
                    });
                    services.AddHostedService<StartupMigrator>();
                });

    }

    public class DataStorage : DbContext
    {
        public DbSet<Mail> Mail { get; set; }

        public DataStorage(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var startMessages = new List<Mail>
            {
                new Mail("Stepan", "Boris", "Zdarova"),
                new Mail("Samara", "Moscow", "Privet")
            };
            modelBuilder.Entity<Mail>().HasData(startMessages);
        }
    }

    public class Mail
    {
        public Guid Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Body { get; set; }
        public DateTime CreatedOn { get; set; }

        public Mail(string from, string to, string body)
        {
            Id = Guid.NewGuid();
            From = from;
            To = to;
            Body = body;
            CreatedOn = DateTime.Now;
        }

        protected Mail()
        {

        }

        public override string ToString()
        {
            return $"[{CreatedOn}] From: {From}, To: {To}, Body: {Body}";
        }
    }

    public class StartupMigrator : BackgroundService
    {
        private readonly IServiceProvider _services;
        private readonly ILogger<StartupMigrator> _logger;

        public StartupMigrator(IServiceProvider services, ILogger<StartupMigrator> logger)
        {
            _services = services;
            _logger = logger;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using var scope = _services.CreateScope();
            await using var storage = scope.ServiceProvider.GetService<DataStorage>();
            storage.Database.Migrate();

            
            var mails = storage.Mail.ToList();
            foreach (var mail in mails)
            {
                _logger.LogInformation(mail.ToString());
            }
        }
    }
}
